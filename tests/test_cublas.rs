#![cfg(feature = "nvidia")]

use oc_blas::cublas::Device;
use oc_blas::{Level1BLAS, OCbArray};

#[test]
fn iamax() {
    let mut device = Device::new(0);
    let mut array = OCbArray::new(10);
    let mut result = OCbArray::new(1);
    let answer = {
        let mut array = array.as_mut();
        array.iter_mut().for_each(|x| { *x = rand::random::<f32>(); });
        1 + array.iter().enumerate().max_by(|a, b| a.1.partial_cmp(b.1).unwrap()).unwrap().0
    };
    device.iamax(&array, 1, &mut result).unwrap();
    device.synchronize();
    assert_eq!(result.as_ref()[0] as usize, answer);
}