use std::env;
use std::path::PathBuf;

fn main() {
    if cfg!(feature="nvidia") {
        configure_compile_opt_for_cublas("CUDA_PATH", "cublas_binding.rs")
    }
}

fn configure_compile_opt_for_cublas(env_name: &str, file_name: &str) {
    let (cuda_libs_path, include_dir_path) = {
        let architecture = match (env::consts::OS, env::consts::ARCH) {
            ("windows", "x86_64") => "x64",
            (o, a) => panic!("Your specified target OS({}) or Architecture({}) is not supported.", o, a)
        };

        let cuda_path: PathBuf = PathBuf::from(
            env::var(env_name).expect(
                &format!("Failed to find the environment value; {}. Have you installed CUDA Toolkit?", env_name)
            )
        );
        (
            cuda_path.join("lib").join(architecture),
            cuda_path.join("include")
        )
    };

    bindgen::Builder::default()
        .header(include_dir_path.join("cuda.h").to_str().unwrap())
        .header(include_dir_path.join("cublas.h").to_str().unwrap())
        .rustified_enum("cublasStatus_t")
        .rustified_enum("cudaError")
        .rustified_enum("cudaError_enum")
        .rustified_enum("cublasOperation_t")
        .rustified_enum("cublasPointerMode_t")
        .derive_debug(false)
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .clang_arg(format!("-I{}", include_dir_path.display()))
        .generate()
        .expect("Failed to generate the wrapped headers.")
        .write_to_file(
            PathBuf::from(env::var("OUT_DIR").unwrap()).join(file_name)
        )
        .expect("Couldn't write bindings!");

    println!("cargo:rustc-link-search={}", cuda_libs_path.display());
    println!("cargo:rustc-link-lib=dylib=cuda");
    println!("cargo:rustc-link-lib=dylib=cudart");
    println!("cargo:rustc-link-lib=dylib=cublas");

    println!("cargo:rerun-if-changed={}", include_dir_path.join("cuda.h").display());
    println!("cargo:rerun-if-changed={}", include_dir_path.join("cublas.h").display());
    println!("cargo:rerun-if-env-changed={}", env_name);
    println!("cargo:rerun-if-changed=build.rs");
}