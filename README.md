# ocBLAS

**ocBLAS** provides OpenBLAS and cuBLAS functions.
One of the features of this crate is that it is very easy to switch between the two.
Furthermore, you can use these functions simultaneously.

# Dependants

- Clang
- vcpkg (If you use OpenBLAS functions.)
- OpenBLAS Library (If you use OpenBLAS functions.)
- CUDA toolkit (If you use cuBLAS functions.)
- cuBLAS Library (If you use cuBLAS functions.)


# Why do we need Clang?
This crate generates thin wrapped OpenBLAS or cuBLAS header files using [bindgen], so you need to install Clang, which is a dependency of bindgen.

[bindgen]: https://crates.io/crates/bindgen


# What is needed to prepare to use OpenBLAS functions?

This crate can find OpenBLAS Library through [vcpkg], so you should install OpenBLAS by vcpkg unless you disable OpenBLAS.

To install OpenBLAS by vcpkg, please run the following command.

````
vcpkg install openblas[threads] --triplet x64-windows-static-md
````

[vcpkg]: https://vcpkg.io/en/index.html


# What is needed to prepare to use cuBLAS functions?

If you want to use cuBLAS functions, you should install cuBLAS Library and CUDA toolkit.
You can download cuBLAS Library from [here](https://developer.nvidia.com/cublas) and
CUDA toolkit from [here](https://developer.nvidia.com/cuda-toolkit).
