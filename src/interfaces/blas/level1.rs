use crate::{OCbArray};
use crate::error::OCbError;

pub trait Level1BLAS<T> {
    fn iamax(&mut self, x: &OCbArray<f32>, incx: usize, result: &mut OCbArray<i32>) -> Result<(), OCbError>;
}
