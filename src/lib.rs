#![allow(dead_code)]

pub mod error;
mod interfaces;
mod array;

pub use interfaces::*;
pub use array::*;


#[cfg(feature = "open_blas")]
pub mod open_blas;

#[cfg(feature = "nvidia")]
pub mod cublas;

