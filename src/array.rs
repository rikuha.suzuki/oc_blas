use std::cell::Cell;

#[derive(Copy, Clone)]
pub(crate) struct CUArrayInfo {
    pub(crate) device_id: usize,
    pub(crate) ptr: *mut std::os::raw::c_void,
}

pub struct OCbArray<T> {
    pub(crate) cu_array_info: Cell<Option<CUArrayInfo>>,
    pub(crate) buf: Vec<T>,
}

impl<T> Drop for OCbArray<T> {
    fn drop(&mut self) {
        #[cfg(feature = "nvidia")]
        if let Some(info) = self.cu_array_info.get_mut() {
            let res: Result<(), crate::cublas::error::CError> = unsafe {
                crate::cublas::raw::cudaSetDevice(info.device_id as std::os::raw::c_int).into()
            };
            if let Err(e) = res { eprintln!("{}", e) }

            let res: Result<(), crate::cublas::error::CError> = unsafe {
                crate::cublas::raw::cudaFree(info.ptr).into()
            };
            if let Err(e) = res { eprintln!("{}", e) }
            info.ptr = std::ptr::null_mut();
        }
    }
}

impl<T> OCbArray<T> {
    pub fn new(num: usize) -> Self {
        let mut buf = Vec::new();
        unsafe {
            buf.reserve_exact(num);
            buf.set_len(num);
        }
        Self { cu_array_info: Cell::new(None), buf }
    }

    pub fn len(&self) -> usize {
        self.buf.len()
    }

    pub fn as_ref(&self) -> &[T] {
        #[cfg(feature = "nvidia")]
        if let Some(info) = self.cu_array_info.get() {
            let res: Result<(), crate::cublas::error::CError> = unsafe {
                crate::cublas::raw::cublasGetVector(
                    self.buf.len() as i32, std::mem::size_of::<T>() as i32, info.ptr, 1, self.buf.as_ptr().cast_mut() as *mut _, 1,
                ).into()
            };
            res.unwrap();
        }
        self.buf.as_slice()
    }

    pub fn as_mut(&mut self) -> MutOCbArray<T> {
        let mut ptr = None;
        #[cfg(feature = "nvidia")]
        if let Some(info) = self.cu_array_info.get() {
            let res: Result<(), crate::cublas::error::CError> = unsafe {
                crate::cublas::raw::cublasGetVector(
                    self.buf.len() as i32, std::mem::size_of::<T>() as i32, info.ptr, 1, self.buf.as_ptr().cast_mut() as *mut _, 1,
                ).into()
            };
            res.unwrap();
            ptr = Some(info.ptr);
        }
        MutOCbArray {
            ptr,
            buf: self.buf.as_mut_slice(),
        }
    }

    pub(crate) fn as_ptr(&self) -> *const T {
        self.cu_array_info.get().unwrap().ptr as *const T
    }

    pub(crate) fn as_mut_ptr(&self) -> *mut T {
        self.cu_array_info.get().unwrap().ptr as *mut T
    }
}

pub struct MutOCbArray<'a, T> {
    ptr: Option<*mut std::os::raw::c_void>,
    buf: &'a mut [T],
}

impl<'a, T> Drop for MutOCbArray<'a, T> {
    fn drop(&mut self) {
        #[cfg(feature = "nvidia")]
        if let Some(ptr) = &self.ptr {
            let res: Result<(), crate::cublas::error::CError> = unsafe {
                crate::cublas::raw::cublasSetVector(
                    self.buf.len() as i32, std::mem::size_of::<T>() as i32, self.buf.as_ptr() as *const _, 1, *ptr, 1,
                ).into()
            };
            if let Err(e) = res {
                eprintln!("{}", e)
            }
        }
    }
}

impl<'a, T> MutOCbArray<'a, T> {
    pub fn iter(&self) -> impl Iterator<Item=&T> {
        self.buf.iter()
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item=&mut T> {
        self.buf.iter_mut()
    }
}
