use std::fmt::{Debug, Display, Formatter};

#[cfg(feature = "open_blas")]
use crate::open_blas::error::OError;

#[cfg(feature = "nvidia")]
use crate::cublas::error::CError;

pub struct OCbError {
    inner: Box<dyn std::error::Error>,
}

#[cfg(feature = "open_blas")]
impl From<OError> for OCbError {
    fn from(err: OError) -> Self {
        Self { inner: Box::new(err) }
    }
}

#[cfg(feature = "nvidia")]
impl From<CError> for OCbError {
    fn from(err: CError) -> Self {
        Self { inner: Box::new(err) }
    }
}

impl Debug for OCbError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        <Box<dyn std::error::Error> as Debug>::fmt(&self.inner, f)
    }
}

impl Display for OCbError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        <Box<dyn std::error::Error> as Display>::fmt(&self.inner, f)
    }
}

impl std::error::Error for OCbError {}