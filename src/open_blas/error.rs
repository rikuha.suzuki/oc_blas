#![allow(unused_variables)]

use std::fmt::{Debug, Display, Formatter};

pub struct OError {}

impl Debug for OError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        todo!()
    }
}

impl Display for OError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        todo!()
    }
}

impl std::error::Error for OError {}