pub(crate) mod error;
pub(crate) mod raw;

use crate::{Level1BLAS, OCbArray};
use crate::error::OCbError;
use error::CError;
use crate::array::CUArrayInfo;


pub struct Device {
    device_id: usize,
    handle: raw::cublasHandle_t,
}

impl Drop for Device {
    fn drop(&mut self) {
        unsafe {
            Result::<(), CError>::from(raw::cublasDestroy_v2(self.handle)).unwrap();
        }
    }
}

impl Device {
    pub fn count_devices() -> usize {
        let mut count: std::os::raw::c_int = 0;
        unsafe {
            Result::<(), CError>::from(raw::cudaGetDeviceCount(&mut count as *mut _)).unwrap()
        }
        count as usize
    }

    pub fn new(device_id: usize) -> Device {
        if device_id >= Self::count_devices() {
            panic!("Failed to get the device(ID:{}) because the ID is grater than the number of CUDA devices.", device_id);
        }

        let mut handle: raw::cublasHandle_t = std::ptr::null_mut();
        unsafe {
            Result::<(), CError>::from(raw::cudaSetDevice(device_id as std::os::raw::c_int)).unwrap();
            Result::<(), CError>::from(raw::cublasCreate_v2(&mut handle as *mut _)).unwrap();
            Result::<(), CError>::from(
                raw::cublasSetPointerMode_v2(handle, raw::cublasPointerMode_t::CUBLAS_POINTER_MODE_DEVICE)
            ).unwrap();
        }

        Self { device_id, handle }
    }

    fn alloc_as_needed<T>(&self, array: &OCbArray<T>) {
        if array.cu_array_info.get().is_none() {
            let mut ptr = std::ptr::null_mut();
            unsafe {
                Result::<(), CError>::from(raw::cudaSetDevice(self.device_id as std::os::raw::c_int)).unwrap();
                Result::<(), CError>::from(raw::cudaMalloc(&mut ptr as *mut _, std::mem::size_of::<T>() * array.len())).unwrap();
                Result::<(), CError>::from(raw::cublasSetVector(
                    array.len() as i32, std::mem::size_of::<T>() as i32, array.buf.as_ptr() as *const _, 1, ptr, 1,
                )).unwrap();
            }
            array.cu_array_info.set(Some(CUArrayInfo {
                device_id: self.device_id,
                ptr,
            }));
        }
    }

    pub fn synchronize(&mut self) {
        unsafe {
            Result::<(), CError>::from(raw::cudaSetDevice(self.device_id as std::os::raw::c_int)).unwrap();
            Result::<(), CError>::from(raw::cudaDeviceSynchronize()).unwrap();
        }
    }
}

impl Level1BLAS<f32> for Device {
    fn iamax(&mut self, x: &OCbArray<f32>, incx: usize, result: &mut OCbArray<i32>) -> Result<(), OCbError> {
        self.alloc_as_needed(x);
        self.alloc_as_needed(result);

        let res: Result<(), CError> = unsafe {
            raw::cublasIsamax_v2(
                self.handle,
                x.len() as std::os::raw::c_int,
                x.as_ptr(),
                incx as std::os::raw::c_int,
                result.as_mut_ptr(),
            ).into()
        };

        res.map_err(|e| OCbError::from(e))
    }
}