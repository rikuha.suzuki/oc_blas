#![allow(dead_code, non_upper_case_globals, non_snake_case, non_camel_case_types)]

use std::fmt::{Debug, Display, Formatter};
use crate::cublas::error::CError;

include!(concat!(env!("OUT_DIR"), "/cublas_binding.rs"));

impl Debug for cublasStatus_t {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let msg = match self {
            cublasStatus_t::CUBLAS_STATUS_SUCCESS => "The cuBLAS process was successful",
            cublasStatus_t::CUBLAS_STATUS_NOT_INITIALIZED => "The cuBLAS library was not initialized. This is usually caused by the lack of a prior cublasCreate() call, an error in the CUDA Runtime API called by the cuBLAS routine, or an error in the hardware setup.",
            cublasStatus_t::CUBLAS_STATUS_ALLOC_FAILED => "Resource allocation failed inside the cuBLAS library. This is usually caused by a cudaMalloc() failure. ",
            cublasStatus_t::CUBLAS_STATUS_INVALID_VALUE => "An unsupported value or parameter was passed to the function (a negative vector size, for example).",
            cublasStatus_t::CUBLAS_STATUS_ARCH_MISMATCH => "The function requires a feature absent from the device architecture; usually caused by compute capability lower than 5.0.",
            cublasStatus_t::CUBLAS_STATUS_MAPPING_ERROR => "An access to GPU memory space failed, which is usually caused by a failure to bind a texture.",
            cublasStatus_t::CUBLAS_STATUS_EXECUTION_FAILED => "The GPU program failed to execute. This is often caused by a launch failure of the kernel on the GPU, which can be caused by multiple reasons.",
            cublasStatus_t::CUBLAS_STATUS_INTERNAL_ERROR => "An internal cuBLAS operation failed. This error is usually caused by a cudaMemcpyAsync() failure.",
            cublasStatus_t::CUBLAS_STATUS_NOT_SUPPORTED => "The functionality requested is not supported",
            cublasStatus_t::CUBLAS_STATUS_LICENSE_ERROR => "The functionality requested requires some license and an error was detected when trying to check the current licensing. This error can happen if the license is not present or is expired or if the environment variable NVIDIA_LICENSE_FILE is not set properly.",
        };
        f.write_str(msg)
    }
}

impl Display for cublasStatus_t {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let msg = match self {
            cublasStatus_t::CUBLAS_STATUS_SUCCESS => "SUCCESS",
            cublasStatus_t::CUBLAS_STATUS_NOT_INITIALIZED => "NOT_INITIALIZED",
            cublasStatus_t::CUBLAS_STATUS_ALLOC_FAILED => "ALLOC_FAILED",
            cublasStatus_t::CUBLAS_STATUS_INVALID_VALUE => "INVALID_VALUE",
            cublasStatus_t::CUBLAS_STATUS_ARCH_MISMATCH => "ARCH_MISMATCH",
            cublasStatus_t::CUBLAS_STATUS_MAPPING_ERROR => "MAPPING_ERROR",
            cublasStatus_t::CUBLAS_STATUS_EXECUTION_FAILED => "EXECUTION_FAILED",
            cublasStatus_t::CUBLAS_STATUS_INTERNAL_ERROR => "INTERNAL_ERROR",
            cublasStatus_t::CUBLAS_STATUS_NOT_SUPPORTED => "NOT_SUPPORTED",
            cublasStatus_t::CUBLAS_STATUS_LICENSE_ERROR => "LICENSE_ERROR",
        };
        f.write_str(msg)
    }
}

impl From<cublasStatus_t> for Result<(), CError> {
    fn from(value: cublasStatus_t) -> Self {
        match value {
            cublasStatus_t::CUBLAS_STATUS_SUCCESS => Ok(()),
            e => Err(CError::from(e))
        }
    }
}

impl std::error::Error for cublasStatus_t {}


impl Debug for cudaError_t {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let msg = unsafe {
            std::ffi::CStr::from_ptr(cudaGetErrorString(*self))
                .to_str()
                .unwrap()
        };
        f.write_str(msg)
    }
}

impl Display for cudaError_t {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let msg = unsafe {
            std::ffi::CStr::from_ptr(cudaGetErrorName(*self))
                .to_str()
                .unwrap()
        };
        f.write_str(msg)
    }
}

impl From<cudaError_t> for Result<(), CError> {
    fn from(value: cudaError_t) -> Self {
        match value {
            cudaError_t::cudaSuccess => Ok(()),
            e => Err(CError::from(e))
        }
    }
}

impl std::error::Error for cudaError_t {}
