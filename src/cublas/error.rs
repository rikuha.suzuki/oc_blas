use std::fmt::{Debug, Display, Formatter};
use crate::cublas::raw;


pub struct CError {
    inner: Box<dyn std::error::Error>,
}

impl From<raw::cublasStatus_t> for CError {
    fn from(err: raw::cublasStatus_t) -> Self {
        Self { inner: Box::new(err) }
    }
}

impl From<raw::cudaError_t> for CError {
    fn from(err: raw::cudaError_t) -> Self {
        Self { inner: Box::new(err) }
    }
}

impl Debug for CError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        <Box<dyn std::error::Error> as Debug>::fmt(&self.inner, f)
    }
}

impl Display for CError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        <Box<dyn std::error::Error> as Display>::fmt(&self.inner, f)
    }
}

impl std::error::Error for CError {}